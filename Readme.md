# README

This repository will contain finished snakemake tutorials from Fenna Feenstra. 

Each tutorial will have its own readme explaining shortly what the tutorial is for, what data it used, which packages were used and what its scripts and outputs are. 

Each tutorial will also contain its respective tutorial file in HTML format and virtual environment, as to avoid confusion to which version of tutorial was used and to have the full running environment of the code.

Name; Casper Peters

St.NO; 341942

Class; BFV3

Tutorials can be found under their respective folders.