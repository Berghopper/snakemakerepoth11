# Tutorial 5

In this tutorial, we learn how to call scripts with multiple threads, run it on condor and logging

## Data used:

* Data from tutorial 2 (in data folder)

## Tutorial files:

* OpenQuestions.txt - txt containing some answers to the questsions
* coresbin109.png - picture of cores, threads and sockets on bin109
* BenchmarkSnakefile - snakefile for benchmarking
* Condor/CondorSnakefile - condor snakefile for condor exercise
* Condor/CondorSnakemake.sh - condor shell script for condor exercise
* Condor/condor.cfg - condor config for condor exercise
* Logging/LoggingSnakefile - snakefile for the logging exercise

## Snakefile outputs:

* tutorial 2 output in multiple folders - this can practically be ignored (mapped_reads, sorted_reads)
* new_benchmarks - new benchmark tests after improvements
* old_benchmarks - old benchmark tests before improvements
* calls/all.vcf - report file attachment
* out.html - report file
* Condor/condor.err - condor error log
* Condor/condor.log - condor log
* Logging/logs/* - all seperate logging files for all rules, summary can be found in OpenQuestions.txt


## Used packages:

(output obtained with ```pip freeze --local``` while in the activated virtual env of the tutorial.)

```
appdirs==1.4.3
biopython==1.70
certifi==2018.1.18
chardet==3.0.4
ConfigArgParse==0.13.0
datrie==0.7.1
docutils==0.14
idna==2.6
numpy==1.14.1
pkg-resources==0.0.0
psutil==5.4.3
PyYAML==3.12
ratelimiter==1.2.0.post0
requests==2.18.4
scipy==1.0.0
snakemake==4.7.0
urllib3==1.22
wrapt==1.10.11

```
