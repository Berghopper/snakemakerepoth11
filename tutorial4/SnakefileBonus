'''
#!/bin/bash

# Inputs:
#      The input file
#      The genomic reference
# Outputs:
#      The .bcf output file

bwa index reference.fa
bwa aln -I -t 8 reference.fa reads.txt > out.sai
bwa samse reference.fa out.sai reads.txt > out.sam

samtools view -bSu out.sam  | samtools sort -  out.sorted

java -Xmx1g -jar /usr/local/picard-tools/MarkDuplicates.jar \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT=out.sorted.bam \
                            OUTPUT=out.dedupe.bam 

samtools index out.dedupe.bam

samtools mpileup -uf reference.fa out.dedupe.bam | bcftools view -bvcg - > out.bcf
'''

# snakefile for the bonus asignment

# constants
REFERENCE="reference.fa"
READS="reads.txt"

# make a bwa index
rule mk_bwa_index:
	input:
		REFERENCE
	output:
		"some_bwa_index.txt"
	shell:
		"bwa index {input} && touch {output}"

# align bwa
rule align_bwa:
	input:
		"some_bwa_index.txt"
	output:
		"out.sai"
	shell:
		"bwa aln -I -t 8 {REFERENCE} {READS} > {output}"
		
# Convert the alignment into a .sam file
rule convert_alignment:
	input:
		"out.sai"
	output:
		"out.sam"
	shell:
		"bwa samse {REFERENCE} {input} {READS} > {output}"
		
# Convert the .sam file into a .bam file and sort it
rule convert_sam_to_bam:
	input:
		"out.sam"
	output:
		"out.sorted.bam"
	shell:
		"samtools view -bSu {input}  | samtools sort -  {output}"
		
# Detect and remove duplicates
rule remove_dups:
	input:
		"out.sorted.bam"
	output:
		"out.dedupe.bam"
	shell:
		"java -Xmx1g -jar /usr/local/picard-tools/MarkDuplicates.jar \
                            MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000\
                            METRICS_FILE=out.metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            VALIDATION_STRINGENCY=LENIENT \
                            INPUT={input} \
                            OUTPUT={output}"
# Index the results
rule idx_res:
	input:
		"out.dedupe.bam"
	output:
		"indexed.txt"
	shell:
		"samtools index {input}"

# create the pileup and convert it into a .bcf file
rule pileup:
	input:
		outdup="out.dedupe.bam",
		indexed="indexed.txt"
	output:
		"out.bcf"
	shell:
		"samtools mpileup -uf {REFERENCE} {input.outdup} | bcftools view -bvcg - > {output}"
		
