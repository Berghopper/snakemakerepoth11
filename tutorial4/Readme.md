# Tutorial 4

In this tutorial, we learn how to call scripts from outside snakemake.

## Data used:

* The gene expression data from Fenna Feenstra; ```/commons/Themas/Thema11/Dataprocessing/WC04/data``` copied to the ```WC04_data``` folder.

## Tutorial files with code:

* Snakefile - runs 'pipeline' with R script. Actualy, it only calls the R script.
* visualize.R - makes a heatmap with the correct gene expression file.
* SnakefileBonus - partial finished bonus; should work, not tested, I do not have any test files...

## Snakefile outputs:

* heatmap.png - heatmap of the gene expression data.

## Used packages:

(output obtained with ```pip freeze --local``` while in the activated virtual env of the tutorial.)

```
appdirs==1.4.3
certifi==2018.1.18
chardet==3.0.4
ConfigArgParse==0.13.0
datrie==0.7.1
idna==2.6
pkg-resources==0.0.0
PyYAML==3.12
ratelimiter==1.2.0.post0
requests==2.18.4
snakemake==4.7.0
urllib3==1.22
wrapt==1.10.11
```
