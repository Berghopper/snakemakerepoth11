# this file is for Miscellaneous rules that might be used everywhere or is not clearly defined

# global functions
def get_files_from_dir(directory, extension):
    """
    read files to list from dir because snakemake is a sad potato that can't handle shell wildcards...
    and I don't want to have to specify EVERY intermediary input/output file...
    """
    files = []
    for i in os.listdir(directory):
        if (directory+i).endswith(extension):
            files.append(directory+i)
    return files

# rule for making zipped output of the R analysis.
rule zip_output:
    input:
        config["empty_files_dir"]+"brain_analyse_done.txt",
        config["empty_files_dir"]+"yolk_analyse_done.txt",
        inputdir = config["r_compare_output_dir"]
    output:
        config["zip_output"]
    shell:
        "zip -r {output} {input.inputdir}"
