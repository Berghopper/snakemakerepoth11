# snakefile for aligning all samples

# rule for aligning with hisat2
# example command; hisat2 -p 7 -x ./hisat2/genome -U 1.fastq,2.fastq,3.fastq,4.fastq --trim5 0 --trim3 0 --n-ceil L,0,0.15 --pen-cansplice 0 --pen-noncansplice 12 --pen-canintronlen G,-8,1 --pen-noncanintronlen G,-8,1 --min-intronlen 20 --max-intronlen 500000 --mp 6,2 --sp 2,1 --np 1 --rdg 5,3 --rfg 5,3 --score-min L,0.0,-0.2 -S Alignment/hisat2/output.sam
rule hisat2_align:
    input:
        config["empty_files_dir"]+"hisat2_index_done.txt"
    output:
        # define all the outputs
        touch(config["empty_files_dir"]+"hisat2_align_done.txt"),
        scrna_brain = config["base_align_output_dir"]+"hisat2/scrna_brain.sam",
        scrna_yolk = config["base_align_output_dir"]+"hisat2/scrna_yolk.sam",
        rna_brain = config["base_align_output_dir"]+"hisat2/rna_brain.sam",
        rna_yolk = config["base_align_output_dir"]+"hisat2/rna_yolk.sam"
    params:
        maincommand  = config['hisat2_align'],
        index = config['hisat2_index']+"/genome",
        h2_trim5 = config["h2_trim5"],
        h2_trim3 = config["h2_trim3"],
        h2_n_ceil = config["h2_n_ceil"],
        h2_pen_cansplice = config["h2_pen_cansplice"],
        h2_pen_noncansplice = config["h2_pen_noncansplice"],
        h2_pen_canintronlen = config["h2_pen_canintronlen"],
        h2_pen_noncanintronlen = config["h2_pen_noncanintronlen"],
        h2_min_intronlen = config["h2_min_intronlen"],
        h2_max_intronlen = config["h2_max_intronlen"],
        h2_mp = config["h2_mp"],
        h2_sp = config["h2_sp"],
        h2_np = config["h2_np"],
        h2_rdg = config["h2_rdg"],
        h2_rfg = config["h2_rfg"],
        h2_score_min = config["h2_score_min"]
    threads: int(config["hisat2_align_threads"])
    run:
        # first get all sample files together, because it has to be joined with commas this is not actually a file.
        scrna_brain = ",".join(get_files_from_dir(config["scrna_samples"]["brain_dir"], ".fastq"))
        scrna_yolk = ",".join(get_files_from_dir(config["scrna_samples"]["yolk_dir"], ".fastq"))
        rna_brain = ",".join(get_files_from_dir(config["rna_samples"]["brain_dir"], ".fastq"))
        rna_yolk = ",".join(get_files_from_dir(config["rna_samples"]["yolk_dir"], ".fastq"))
        # for both single and multicell - for both yolk and brain, run hisat2.
        # format things that will be universal for all runs, then the left over ones will be formatted again (these have double curly brackets).
        global_command = "{maincommand} -p {my_threads} -x {hisat_index} -U {{input_files}} "+\
        "--trim5 {trim5} "+\
        "--trim3 {trim3} "+\
        "--n-ceil {n_ceil} "+\
        "--pen-cansplice {pen_cansplice} "+\
        "--pen-noncansplice {pen_noncansplice} "+\
        "--pen-canintronlen {pen_canintronlen} "+\
        "--pen-noncanintronlen {pen_noncanintronlen} "+\
        "--min-intronlen {min_intronlen} "+\
        "--max-intronlen {max_intronlen} "+\
        "--mp {mp} "+\
        "--sp {sp} "+\
        "--np {np} "+\
        "--rdg {rdg} "+\
        "--rfg {rfg} "+\
        "--score-min {score_min} "+\
        "-S {{output_file}}"
        global_command = global_command.format(
        maincommand=params.maincommand,
        my_threads=threads,
        hisat_index=params.index,
        trim5=params.h2_trim5,
        trim3=params.h2_trim3,
        n_ceil=params.h2_n_ceil,
        pen_cansplice=params.h2_pen_cansplice,
        pen_noncansplice=params.h2_pen_noncansplice,
        pen_canintronlen=params.h2_pen_canintronlen,
        pen_noncanintronlen=params.h2_pen_noncanintronlen,
        min_intronlen=params.h2_min_intronlen,
        max_intronlen=params.h2_max_intronlen,
        mp=params.h2_mp,
        sp=params.h2_sp,
        np=params.h2_np,
        rdg=params.h2_rdg,
        rfg=params.h2_rfg,
        score_min=params.h2_score_min
        )
        print(global_command)
        # format for each case and run
        shell(global_command.format(input_files=scrna_brain, output_file=output.scrna_brain))
        shell(global_command.format(input_files=scrna_yolk, output_file=output.scrna_yolk))
        shell(global_command.format(input_files=rna_brain, output_file=output.rna_brain))
        shell(global_command.format(input_files=rna_yolk, output_file=output.rna_yolk))

# example command; bwa mem -t 7 -k 19 -w 100 -d 100 -r 1.5 -c 500 -D 0.5 -m 50 -W 0 -A 1 -B 4 -O 6 -E 1 -L 5 -U 17 Indeces/mm10/bwa/ SRR3319604.fastq > output0.sam
rule bwa_align:
    input:
        config["empty_files_dir"]+"bwa_index_done.txt"
    output:
        # define all the outputs
        touch(config["empty_files_dir"]+"bwa_align_done.txt"),
        # also define rough output directories.
        scrna_brain = config["base_align_output_dir"]+"bwa/scrna_brain/",
        scrna_yolk = config["base_align_output_dir"]+"bwa/scrna_yolk/",
        rna_brain = config["base_align_output_dir"]+"bwa/rna_brain/",
        rna_yolk = config["base_align_output_dir"]+"bwa/rna_yolk/"
    params:
        maincommand  = config['bwa_align'],
        index = config['bwa_index']+"/genome",
        bwa_k = config["bwa_k"],
        bwa_w = config["bwa_w"],
        bwa_d = config["bwa_d"],
        bwa_r = config["bwa_r"],
        bwa_c = config["bwa_c"],
        bwa_D = config["bwa_D"],
        bwa_m = config["bwa_m"],
        bwa_W = config["bwa_W"],
        bwa_A = config["bwa_A"],
        bwa_B = config["bwa_B"],
        bwa_O = config["bwa_O"],
        bwa_E = config["bwa_E"],
        bwa_L = config["bwa_L"],
        bwa_U = config["bwa_U"]
    threads: int(config["bwa_align_threads"])
    run:
        # first get all sample files together.
        scrna_brain = get_files_from_dir(config["scrna_samples"]["brain_dir"], ".fastq")
        scrna_yolk = get_files_from_dir(config["scrna_samples"]["yolk_dir"], ".fastq")
        rna_brain = get_files_from_dir(config["rna_samples"]["brain_dir"], ".fastq")
        rna_yolk = get_files_from_dir(config["rna_samples"]["yolk_dir"], ".fastq")
        # for both single and multicell - for both yolk and brain, run bwa, each file has to be done seperately.
        # format things that will be universal for all runs, then the left over ones will be formatted again (these have double curly brackets).
        global_command = "{the_command} -t {my_threads} "+\
        "-k {k} "+\
        "-w {w} "+\
        "-d {d} "+\
        "-r {r} "+\
        "-c {c} "+\
        "-D {D} "+\
        "-m {m} "+\
        "-W {W} "+\
        "-A {A} "+\
        "-B {B} "+\
        "-O {O} "+\
        "-E {E} "+\
        "-L {L} "+\
        "-U {U} "+\
        "{bwa_index} {{input_file}} > {{output_file}}"
        global_command = global_command.format(
        the_command=params.maincommand,
        my_threads=threads,
        k=params.bwa_k,
        w=params.bwa_w,
        d=params.bwa_d,
        r=params.bwa_r,
        c=params.bwa_c,
        D=params.bwa_D,
        m=params.bwa_m,
        W=params.bwa_W,
        A=params.bwa_A,
        B=params.bwa_B,
        O=params.bwa_O,
        E=params.bwa_E,
        L=params.bwa_L,
        U=params.bwa_U,
        bwa_index=params.index
        )
        # run for each sample in each group
        for sample in scrna_brain:
            sample = sample.split("/")[-1]
            shell(global_command.format(input_file=config["scrna_samples"]["brain_dir"]+sample, output_file=output.scrna_brain+sample+".sam"))
        for sample in scrna_yolk:
            sample = sample.split("/")[-1]
            shell(global_command.format(input_file=config["scrna_samples"]["yolk_dir"]+sample, output_file=output.scrna_yolk+sample+".sam"))
        for sample in rna_brain:
            sample = sample.split("/")[-1]
            shell(global_command.format(input_file=config["rna_samples"]["brain_dir"]+sample, output_file=output.rna_brain+sample+".sam"))
        for sample in rna_yolk:
            sample = sample.split("/")[-1]
            shell(global_command.format(input_file=config["rna_samples"]["yolk_dir"]+sample, output_file=output.rna_yolk+sample+".sam"))

# example command; STARlong --runThreadN 7 --genomeDir Indeces/mm10/star/ --readFilesIn SRR3319604.fastq --outFileNamePrefix Alignment/star/output0  --outFilterMultimapNmax 20 --alignSJoverhangMin 8 --alignSJDBoverhangMin 1 --outFilterMismatchNmax 999 --outFilterMismatchNoverReadLmax 0.04 --alignIntronMin 20 --alignIntronMax 1000000 --alignMatesGapMax 1000000
rule star_align:
    input:
        config["empty_files_dir"]+"star_index_done.txt"
    output:
        # define all the outputs
        touch(config["empty_files_dir"]+"star_align_done.txt"),
        scrna_brain = config["base_align_output_dir"]+"star/scrna_brain/",
        scrna_yolk = config["base_align_output_dir"]+"star/scrna_yolk/",
        rna_brain = config["base_align_output_dir"]+"star/rna_brain/",
        rna_yolk = config["base_align_output_dir"]+"star/rna_yolk/"
    params:
        maincommand  = config['star_align'],
        index = config['star_index'],
        st_outFilterMultimapNmax = config["st_outFilterMultimapNmax"],
        st_alignSJoverhangMin = config["st_alignSJoverhangMin"],
        st_alignSJDBoverhangMin= config["st_alignSJDBoverhangMin"],
        st_outFilterMismatchNmax = config["st_outFilterMismatchNmax"],
        st_outFilterMismatchNoverReadLmax = config["st_outFilterMismatchNoverReadLmax"],
        st_alignIntronMin = config["st_alignIntronMin"],
        st_alignIntronMax = config["st_alignIntronMax"],
        st_alignMatesGapMax = config["st_alignMatesGapMax"]
    threads: int(config["star_align_threads"])
    run:
        # first get all sample files together.
        scrna_brain = get_files_from_dir(config["scrna_samples"]["brain_dir"], ".fastq")
        scrna_yolk = get_files_from_dir(config["scrna_samples"]["yolk_dir"], ".fastq")
        rna_brain = get_files_from_dir(config["rna_samples"]["brain_dir"], ".fastq")
        rna_yolk = get_files_from_dir(config["rna_samples"]["yolk_dir"], ".fastq")
        # for both single and multicell - for both yolk and brain, run star, each file has to be done seperately.
        # format things that will be universal for all runs, then the left over ones will be formatted again (these have double curly brackets).
        global_command = "cd {{outputdir}} && "+\
        "{the_command} --runThreadN {my_threads} --genomeDir {star_index} "+\
        "--readFilesIn {{input_file}} --outFileNamePrefix {{output_prefix}} "+\
        "--outFilterMultimapNmax {outFilterMultimapNmax} "+\
        "--alignSJoverhangMin {alignSJoverhangMin} "+\
        "--alignSJDBoverhangMin {alignSJDBoverhangMin} "+\
        "--outFilterMismatchNmax {outFilterMismatchNmax} "+\
        "--outFilterMismatchNoverReadLmax {outFilterMismatchNoverReadLmax} "+\
        "--alignIntronMin {alignIntronMin} "+\
        "--alignIntronMax {alignIntronMax} "+\
        "--alignMatesGapMax {alignMatesGapMax}"
        global_command = global_command.format(
        the_command=params.maincommand,
        my_threads=threads,
        star_index=params.index,
        outFilterMultimapNmax=params.st_outFilterMultimapNmax,
        alignSJoverhangMin=params.st_alignSJoverhangMin,
        alignSJDBoverhangMin=params.st_alignSJDBoverhangMin,
        outFilterMismatchNmax=params.st_outFilterMismatchNmax,
        outFilterMismatchNoverReadLmax=params.st_outFilterMismatchNoverReadLmax,
        alignIntronMin=params.st_alignIntronMin,
        alignIntronMax=params.st_alignIntronMax,
        alignMatesGapMax=params.st_alignMatesGapMax
        )
        # run for each sample in each group
        for sample in scrna_brain:
            sample = sample.split("/")[-1]
            shell(global_command.format(outputdir=output.scrna_brain, input_file=config["scrna_samples"]["brain_dir"]+sample, output_prefix=output.scrna_brain+sample+"_"))
        for sample in scrna_yolk:
            sample = sample.split("/")[-1]
            shell(global_command.format(outputdir=output.scrna_yolk, input_file=config["scrna_samples"]["yolk_dir"]+sample, output_prefix=output.scrna_yolk+sample+"_"))
        for sample in rna_brain:
            sample = sample.split("/")[-1]
            shell(global_command.format(outputdir=output.rna_brain, input_file=config["rna_samples"]["brain_dir"]+sample, output_prefix=output.rna_brain+sample+"_"))
        for sample in rna_yolk:
            sample = sample.split("/")[-1]
            shell(global_command.format(outputdir=output.rna_yolk, input_file=config["rna_samples"]["yolk_dir"]+sample, output_prefix=output.rna_yolk+sample+"_"))
