# makes homer tag directories for each sample and quantify it
# example for tag dir creation; Software/Homer/bin/makeTagDirectory /scRNA/bwa_brain_E12.5/Pipeline_2017_11_01_19.25.08_YMDHMS/RNA_quantification/homer/HOMERTEMPTAGDIR output0.sam output1.sam output2.sam output3.sam
# exmaple quantify; analyzeRepeats.pl rna mm10 -d HOMERTAGDIR -maxdiv 1.0 -mindiv 0 -minLength 0  -minLengthP 0 -maxLengthP 100 -count 3utr -strand both -pc 0 -condenseGenes -raw  > output.txt
tagdir_command = "cd {homer_bindir} && {my_tagdir_command} {tag_dir} {the_input_files}"
quantify_command = "cd {{homer_bindir}} && perl {{my_quantify_command}} "+\
                   "{file_type} "+\
                   "{genome} "+\
                   "-d {{tag_dir}} "+\
                   "-maxdiv {maxdiv} "+\
                   "-mindiv {mindiv} "+\
                   "-minLength {minLength} "+\
                   "-minLengthP {minLengthP} "+\
                   "-maxLengthP {maxLengthP} "+\
                   "-count {count} "+\
                   "-strand {strand} "+\
                   "-pc {pc} "+\
                   "-condenseGenes -raw > {{quantify_output}}"
quantify_command = quantify_command.format(
                    file_type=config["hom_type"],
                    genome=config["hom_genome"],
                    maxdiv=config["hom_maxdiv"],
                    mindiv=config["hom_mindiv"],
                    minLength=config["hom_minLength"],
                    minLengthP=config["hom_minLengthP"],
                    maxLengthP=config["hom_maxLengthP"],
                    count=config["hom_count"],
                    strand=config["hom_strand"],
                    pc=config["hom_pc"]
                   )

# for each aligner, type and sample quantify the sam files.

#HISAT2
rule homer_scrna_hisat_brain:
    input:
        config["empty_files_dir"]+"hisat2_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/scrna/hisat2_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/scrna/hisat2_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = config["base_align_output_dir"]+"hisat2/scrna_brain.sam"
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_scrna_hisat_yolk:
    input:
        config["empty_files_dir"]+"hisat2_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/scrna/hisat2_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/scrna/hisat2_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = config["base_align_output_dir"]+"hisat2/scrna_yolk.sam"
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_hisat_brain:
    input:
        config["empty_files_dir"]+"hisat2_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/rna/hisat2_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/rna/hisat2_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = config["base_align_output_dir"]+"hisat2/rna_brain.sam"
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_hisat_yolk:
    input:
        config["empty_files_dir"]+"hisat2_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/rna/hisat2_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/rna/hisat2_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = config["base_align_output_dir"]+"hisat2/rna_yolk.sam"
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

#BWA
rule homer_scrna_bwa_brain:
    input:
        config["empty_files_dir"]+"bwa_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/scrna/bwa_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/scrna/bwa_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"bwa/scrna_brain/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_scrna_bwa_yolk:
    input:
        config["empty_files_dir"]+"bwa_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/scrna/bwa_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/scrna/bwa_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"bwa/scrna_yolk/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_bwa_brain:
    input:
        config["empty_files_dir"]+"bwa_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/rna/bwa_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/rna/bwa_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"bwa/rna_brain/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_bwa_yolk:
    input:
        config["empty_files_dir"]+"bwa_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/rna/bwa_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/rna/bwa_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"bwa/rna_yolk/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

#STAR
rule homer_scrna_star_brain:
    input:
        config["empty_files_dir"]+"star_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/scrna/star_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/scrna/star_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"star/scrna_brain/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_scrna_star_yolk:
    input:
        config["empty_files_dir"]+"star_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/scrna/star_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/scrna/star_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"star/scrna_yolk/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_star_brain:
    input:
        config["empty_files_dir"]+"star_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"brain/rna/star_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"brain/rna/star_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"star/rna_brain/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))

rule homer_rna_star_yolk:
    input:
        config["empty_files_dir"]+"star_align_done.txt"
    output:
        # tag dir output
        tag_dir = config["homer_base_output_dir"]+"yolk/rna/star_tagdir/",
        # quantify output
        quantify_output = config["homer_base_output_dir"]+"yolk/rna/star_output.txt"
    params:
        homer_bindir = config["homer_bindir"]
    threads: 1
    run:
        input_files = " ".join(get_files_from_dir(config["base_align_output_dir"]+"star/rna_yolk/", ".sam"))
        shell(tagdir_command.format(homer_bindir=params.homer_bindir, my_tagdir_command=params.homer_bindir+"makeTagDirectory", tag_dir=output.tag_dir, the_input_files=input_files))
        shell(quantify_command.format(homer_bindir=params.homer_bindir, my_quantify_command=params.homer_bindir+"analyzeRepeats.pl", tag_dir=output.tag_dir, quantify_output=output.quantify_output))
