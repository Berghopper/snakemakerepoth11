# snakefile for making indeces

# rule for concatting genome, some programs don't accept multiple fastas...
# example command; cat 1.txt 2.txt > 3.txt

# concatenate genome fastas into 1 fasta file, this is needed for bwa and hisat2
rule concat_index_fastas:
    input:
        # first get the files neccesary for indexing.
        genome_fastas = get_files_from_dir(config["genome_fasta_dir"], ".fa")
    output:
        # then acquire the output of where the concatted file should go.
        config["concat_output"]
    run:
        # run the concatting step.
        shell("cat {input} > {output}")

# hisat2 index step
# example command; hisat2-build –f ./concat_genome.fa ./genome/
rule generate_hisat2_index:
    input:
        # ask for the concat output - hisat2 needs 1 file for the genome
        config["concat_output"]
    params:
        # specify hisat2 command
        maincommand = config['hisat2_build']
    output:
        # touch a file for showing that index is done
        touch(config["empty_files_dir"]+"hisat2_index_done.txt"),
        # make sure the index directory is made.
        index = config["hisat2_index"]
    threads: int(config["hisat2_build_threads"])
    shell:
        # run indexing step
        "{params.maincommand} -p {threads} -f {input} {output.index}/genome"

# BWA index step
# example command; bwa index -p ./bwa/ ./concat_genome.fa
rule generate_bwa_index:
    input:
        # ask for the concat output - bwa needs 1 file for the genome
        config["concat_output"]
    params:
        maincommand = config['bwa_build']
    output:
        # touch a file for showing that index is done
        touch(config["empty_files_dir"]+"bwa_index_done.txt"),
        # make sure the index directory is made.
        index = config["bwa_index"]
    # bwa does not run on multiple cores, so only give 1 thread
    threads: 1
    shell:
        # run indexing step
        "{params.maincommand} -p {output.index}/genome {input}"

# STAR index step
# example command; ./STAR/bin/Linux_x86_64_static/STAR --runMode genomeGenerate --runThreadN 8 --genomeDir ./star --genomeSAindexNbases 10 --genomeChrBinNbits 14 --genomeFastaFiles ./chr1.fa ./chr10.fa ./chr11.fa
rule generate_star_index:
    input:
        # with star, instead get all the genome files.
        genome_fastas = get_files_from_dir(config["genome_fasta_dir"], ".fa")
    params:
        # get all the params
        maincommand = config['star_build'],
        genomeSAindexNbases = config["star_genomeSAindexNbases"],
        genomeChrBinNbits = config["star_genomeChrBinNbits"]
    output:
        # touch a file for showing that index is done.
        touch(config["empty_files_dir"]+"star_index_done.txt"),
        # make sure that the index dir is made.
        index = config["star_index"]
    threads: int(config["star_build_threads"])
    # with star - cd to index directory first - it creates it's own log stubbornly.
    shell:
        "cd {output.index} && {params.maincommand} --runMode genomeGenerate --runThreadN {threads} --genomeDir {output.index} --genomeSAindexNbases {params.genomeSAindexNbases} --genomeChrBinNbits {params.genomeChrBinNbits} --genomeFastaFiles {input}"
