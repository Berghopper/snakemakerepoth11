# rules for R scripts
#./scsaAnalyse.R scRNA/bwa_brain_E12.5/RNA_quantification.txt,RNA/hisat2_brain_E12.5/RNA_quantification.txt r_compare/brain_E12.5_bwa_hisat2/ 8 scRNA,RNA

# sadly because the school computers don't have the 'pheatmap' R library, the sortpipelines.R script cannot be run.
#./sortPipelines.R /data/storage/students/ccpeters_cawarmerdam/scsa/Processed_data/Pipeline_runs/Final_runs//r_compare/

# set command here, it is used in multiple rules
analyse_script = "Rscript {analyse_script_command} {my_input_files} {my_outputdir} {collumn} {order}"

# rule for the brain samples
rule analyse_brain:
    input:
        # define scrna and rna groups seperate as to loop through easier later on.
        scrna_brain_homer_samples = [
            rules.homer_scrna_hisat_brain.output.quantify_output,
            rules.homer_scrna_bwa_brain.output.quantify_output,
            rules.homer_scrna_star_brain.output.quantify_output
        ],
        rna_brain_homer_samples = [
            rules.homer_rna_hisat_brain.output.quantify_output,
            rules.homer_rna_bwa_brain.output.quantify_output,
            rules.homer_rna_star_brain.output.quantify_output
        ]
    output:
        # make a finished flag file
        touch(config["empty_files_dir"]+"brain_analyse_done.txt"),
        # set up output dir for this r script.
        outputdir = config["r_compare_output_dir"]
    # always run on 1 thread, multiple isnt possible.
    threads: 1
    run:
        # loop through both samples
        for scrna_homer_file in input.scrna_brain_homer_samples:
            for rna_homer_file in input.rna_brain_homer_samples:
                # get aligner types so the directories can be specified properly.
                scrna_homer_file_aligner = scrna_homer_file.split("/")[-1].split("_")[0]
                rna_homer_file_aligner = rna_homer_file.split("/")[-1].split("_")[0]
                # run R script with formatted command
                shell(analyse_script.format(
                analyse_script_command=config["analyse_script"],
                my_input_files=scrna_homer_file+","+rna_homer_file,
                my_outputdir=output.outputdir+"brain_E12.5_"+scrna_homer_file_aligner+"_"+rna_homer_file_aligner,
                collumn=config["sA_collumn"],
                order=config["sA_order"]
                ))

# rule for the yolk samples
rule analyse_yolk:
    input:
        # define scrna and rna groups seperate as to loop through easier later on.
        scrna_yolk_homer_samples = [
            rules.homer_scrna_hisat_yolk.output.quantify_output,
            rules.homer_scrna_bwa_yolk.output.quantify_output,
            rules.homer_scrna_star_yolk.output.quantify_output
        ],
        rna_yolk_homer_samples = [
            rules.homer_rna_hisat_yolk.output.quantify_output,
            rules.homer_rna_bwa_yolk.output.quantify_output,
            rules.homer_rna_star_yolk.output.quantify_output
        ]
    output:
        # make a finished flag file
        touch(config["empty_files_dir"]+"yolk_analyse_done.txt"),
        # set up output dir
        outputdir = config["r_compare_output_dir"]
    # always run on 1 thread.
    threads: 1
    run:
        # loop through both samples
        for scrna_homer_file in input.scrna_yolk_homer_samples:
            for rna_homer_file in input.rna_yolk_homer_samples:
                # get aligner types
                scrna_homer_file_aligner = scrna_homer_file.split("/")[-1].split("_")[0]
                rna_homer_file_aligner = rna_homer_file.split("/")[-1].split("_")[0]
                # run R script
                shell(analyse_script.format(
                analyse_script_command=config["analyse_script"],
                my_input_files=scrna_homer_file+","+rna_homer_file,
                my_outputdir=output.outputdir+"yolk_E12.5_"+scrna_homer_file_aligner+"_"+rna_homer_file_aligner,
                collumn=config["sA_collumn"],
                order=config["sA_order"]
                ))
