#!/usr/bin/Rscript

rm(list=ls())

library("pheatmap")
library("RColorBrewer")

args = commandArgs(trailingOnly=TRUE)
if (length(args)!= 1) {
  stop("the directory where comparison summaries are stored must be supplied", call.=FALSE)
}

directory <- args[1]
# in the directory, collect every directory
dirs = list.files(directory, full.names=FALSE)
for (dir in dirs) {
  # first do stuff for the summary.
  mySummary <- paste0(directory, .Platform$file.sep, dir, .Platform$file.sep, "output_summary.csv")
  print(mySummary)


  # if the merged dataset doesn't exist, create it
  if (!exists("dataset")){
    dataset <- read.csv(mySummary, header=TRUE, sep=",", row.names = 1, dec=".")
  } else if (exists("dataset")){
    temp_dataset <- read.csv(mySummary, header=TRUE, sep=",", row.names = 1, dec=".")
    dataset<-cbind(dataset, temp_dataset)
    rm(temp_dataset)
  }
  colnames(dataset)[ncol(dataset)] <- dir

  # secondly do stuff for the complete datasets.
  mybigdataset <- paste0(directory, .Platform$file.sep, dir, .Platform$file.sep, "output_Deseq2.csv")
  # if the merged dataset doesn't exist, create it
  if (!exists("bigdataset")){
    bigdataset <- read.csv(mybigdataset, header=TRUE, sep=",", row.names = 1, dec=".")[,2, drop=F]
    colnames(bigdataset)[1] <- dir
  } else if (exists("dataset")){
    temp_dataset <- read.csv(mybigdataset, header=TRUE, sep=",", row.names = 1, dec=".")[,2, drop=F]
    colnames(temp_dataset)[1] <- dir
    bigdataset<- transform(merge(temp_dataset, bigdataset, by = "row.names", all = TRUE), row.names=Row.names, Row.names=NULL)
    #bigdataset[is.na(bigdataset)] <- 0
    rm(temp_dataset)
  }

}
# filter out na's
bigdataset <- na.omit(bigdataset)

# transform and order on median
dataset <- t(dataset)
newdata <- dataset[order(abs(dataset[,"Median"])),]

# write to csv
write.csv(newdata, paste0(directory, .Platform$file.sep, "sorted_pipelines.csv"))

# make some colors
colors <- colorRampPalette( (brewer.pal(11,"Spectral")))(256)
# Generate a heatmap for all pileline combinations
svg(paste0(directory, "/output_heatmap.svg"))
pheatmap(bigdataset, col =colors, cluster_cols = F, cex=0.8, show_rownames = F, main = "Log2 foldchanges scRNA/RNA per pipeline combination")
dev.off()

# make boxplots for every pipeline combi

svg(paste0(directory, "/output_boxplots.svg"))
oldmar <- par()$mar
oldoma <- par()$oma
par(mar=c(5.1, 11, 4.1, 4.1))
boxplot(bigdataset, horizontal = T, las=1)
par(mar=oldmar)
par(oma=oldoma)
dev.off()

# write sessioninfo and traceback to a file
writeLines(capture.output(sessionInfo()), paste0(directory, "/sessionInfo.txt"))
writeLines(capture.output(traceback()), paste0(directory, "/traceback.txt"))
print(getwd())
