#!/usr/bin/Rscript

# run install commands  (un-comment)
#source("https://bioconductor.org/biocLite.R")
#biocLite("DESeq2")

#require(DESeq2)

# describe self-made functions
rm(list=ls())

library(DESeq2)

args = commandArgs(trailingOnly=TRUE)

# test if there are at least 4 arguments: if not, return an error
if (length(args)!= 4) {
  stop("4 arguments must be supplied: count_data files seperated with a comma (2 files, 1 arg, no spaces)\
       , output_folder, the column where count data is stored, and comma seperated conditions in the same order as the given files", call.=FALSE)
}

# get files, conditions, check outputdir
files <- strsplit(args[1], ',')[[1]]
Conditions <- strsplit(args[4], ',')[[1]]
dir.create(file.path(args[2]), showWarnings = FALSE)

# setup vectors for temp storage
Names <- c()
Layouts <- c()
Count_matrix <- matrix(nrow = 0, ncol = 0)

# create meta table, combine read counts.
for (i in seq_len(length(files))) {
  Names <- c(Names, tools::file_path_sans_ext(files[i]))
  Layouts <- c(Layouts, "single-read")
  count <- read.csv(files[i], sep = "\t", row.names = 1)[strtoi(args[3])]
  colnames(count) <- tools::file_path_sans_ext(files[i])
  Count_matrix <- transform(merge(count, Count_matrix, by = "row.names", all = TRUE), row.names=Row.names, Row.names=NULL)
  Count_matrix[is.na(Count_matrix)] <- 0
}

samples <- data.frame(shortname=Names, LibraryLayout=Layouts, countf=files, condition=Conditions)

zero_cds = DESeqDataSetFromMatrix(countData=Count_matrix,
                             colData=samples,
                             design= ~ condition)

# pre filtering
# filter zeroes
cds <- zero_cds[ rowSums(counts(zero_cds)) >= 1, ]

cds <- DESeq(cds, betaPrior = T, parallel = T)
res <- results(cds)

# write output files

write.csv(as.data.frame(res), paste0(args[2],"/output_Deseq2.csv"))

# make a summary file
summary <- quantile(res$log2FoldChange)
summary <- data.frame(summary)
summary[6,1] <- mean(res$log2FoldChange)
row.names(summary) <- c("min", "Q1", "Median", "Q3", "max", "mean")

write.csv(summary, paste0(args[2],"/output_summary.csv"))

# Make some plots at the end

jpeg(paste0(args[2], "/output_boxplot.jpg"))
boxplot(res$log2FoldChange, horizontal = T, main="log2FoldChange boxplot")
dev.off()

# finally, write sessioninfo and traceback to file.
writeLines(capture.output(sessionInfo()), paste0(args[2], "/sessionInfo.txt"))
writeLines(capture.output(traceback()), paste0(args[2], "/traceback.txt"))
print(getwd())

