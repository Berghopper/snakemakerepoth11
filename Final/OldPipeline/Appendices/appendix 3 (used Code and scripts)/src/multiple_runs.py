#!/usr/bin/env python3
"""
This subpackage is meant for providing a python dictionary filled with the commands used to run the pipeline with
the different aligners. This will run all the pipelines in sequence, after which it will analyze the runs.
"""

__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"

# imports
import sys
import os
import argparse
import shutil
import json

# constants
# classes
# functions
def input_parsing():
    """
    Parse command line input.
    """
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-scf', "--scRNA_fastq_folder", type=test_directory, required=True,
                        help="Give the folder/directory with your desired fastq's (single cell) you want to put through"
                             " the pipeline.")
    parser.add_argument('-mcf', "--mcRNA_fastq_folder", type=test_directory, required=True,
                        help="Give the folder/directory with your desired fastq's (multi cell) you want to put through "
                             "the pipeline.")
    parser.add_argument('-o', '--output', type=test_directory, required=True,
                        help="Give the output directory the pipeline will use while Processing all in-between data.")
    parser.add_argument('-c', '--config', type=test_config, required=True)

    args = parser.parse_args()
    return args

def test_config(config):
    """
    Test if config is json file, exists and is readable
    """
    if not config.endswith(".json"):
        raise argparse.ArgumentTypeError("File \"{}\" should be a JSON file with .json extension".format(config))
    if not (os.path.isfile(config) and os.access(config, os.R_OK)):
        raise argparse.ArgumentTypeError("JSON file \"{}\" doesn't exist or isn't readable.".format(config))
    # read in json
    config = parse_config_file(config)
    return config

def parse_config_file(config):
    """
    Assigns dict to global CONFIGURATION given the json config file.
    """
    with open(config.config) as json_config_file:

        try:
            config = json.load(json_config_file)
        except ValueError as e:
            raise argparse.ArgumentTypeError("Config file \"{}\" isn't in JSON format.".format(config.config))

    return config

def test_directory(directory):
    """Check if dir is an existing dir."""
    if not os.path.isdir(directory):
        raise argparse.ArgumentTypeError("Directory \"{}\" is not an existing directory, please input a valid one."
                                         .format(directory))
    return directory+"/"

def recursive_fastq_folder_search(directory):
    """
    This returns all folders with fastq's inside of them. If a folder is found, this function gets called again so it
    can go deeper into the folders.
    """
    fastq_dirs = []
    for item in os.listdir(directory):
        if os.path.isfile(directory + "/" + item) and (item.endswith(".fastq") or item.endswith(".fq")):
            fastq_dirs.append(directory)
        elif os.path.isdir(directory + "/" + item):
            fastq_dirs.extend(recursive_fastq_folder_search(directory + "/" + item))
    # remove duplicates while returning
    return list(set(fastq_dirs))

def run_all_pipelines(rna_inputs, rna_output, config):
    """
    This function runs a pipeline for each found fastq folder.
    """
    rna_outputs = []

    # for each fastq folder, run a pipeline.
    for rna_folder in rna_inputs:
        for aligner, executable in config.items():
            # put aligner and sample in the folder name for later.
            rna_output_tmp = rna_output+"/"+aligner+"_"+rna_folder.split("/")[-1]+"/"
            mkdir_ifnotexists(rna_output_tmp)
            rna_outputs.append(rna_output_tmp)
            executable += " -o \""+rna_output_tmp+"\" -f \""+rna_folder+"\""
            os.system(executable)
    return rna_outputs

def scan_pipelines(scRNA_outputs, mcRNA_outputs, output_rcompare):
    """
    This function calls our "scsaAnalyse.R" for making a scan between each scRNA and RNA pipeline run. This is for
    evaluating the similarity.
    """
    for scRNA_output_folder in scRNA_outputs:
        # define aligner and sample (single cell)
        scRNA_aligner = list(filter(None, scRNA_output_folder.split("/")))[-1].split("_")[0]
        scRNA_sample = "_".join(list(filter(None, scRNA_output_folder.split("/")))[-1].split("_")[1:])
        for mcRNA_output_folder in mcRNA_outputs:
            # define aligner and sample (multi cell)
            mcRNA_aligner = list(filter(None, mcRNA_output_folder.split("/")))[-1].split("_")[0]
            mcRNA_sample = "_".join(list(filter(None, mcRNA_output_folder.split("/")))[-1].split("_")[1:])
            # if samples are equal, compare.
            if scRNA_sample == mcRNA_sample:
                # first get homer files.
                # single cell (sc_homertxt)
                sc_homerdir = scRNA_output_folder + "/" + os.listdir(scRNA_output_folder)[
                    0] + "/RNA_quantification/homer/"
                sc_homertxt = False
                for item in os.listdir(sc_homerdir):
                    if item.endswith(".txt"):
                        sc_homertxt = sc_homerdir + item
                # multi cell (mc_homertxt)
                mc_homerdir = mcRNA_output_folder + "/" + os.listdir(mcRNA_output_folder)[
                    0] + "/RNA_quantification/homer/"
                mc_homertxt = False
                for item in os.listdir(mc_homerdir):
                    if item.endswith(".txt"):
                        mc_homertxt = mc_homerdir + item

                # run R script if both homer txt's are found
                if sc_homertxt and mc_homertxt:
                    r_commasep = sc_homertxt + "," + mc_homertxt
                    r_output = output_rcompare+scRNA_sample + "_" + scRNA_aligner + "_" + mcRNA_aligner + "/"
                    mkdir_ifnotexists(r_output)
                    exec_string = "./scsaAnalyse.R " + r_commasep + " " + r_output + " 8 scRNA,RNA"
                    print("Trying to run R script with; "+exec_string)
                    os.system(exec_string)
                else:
                    sys.exit("oops, something went wrong when analysing")

def sort_pipeline_scans(output_rcompare):
    """
    This function sorts all the scans into 1 file under the "rcompare" folder. This way we can evaluate the best
    combination of aligners/mappers with single and multicell data.
    """
    exec_string = "./sortPipelines.R "+output_rcompare
    print("Trying to run R script with; " + exec_string)
    os.system(exec_string)

def mkdir_ifnotexists(directory):
    """
    Makes dir if it does not exist.
    """
    exists = True
    if not os.path.exists(directory):
        os.makedirs(directory)
        exists = False
    return exists

def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    # first get every directory for scRNA dir and mcRNA seperately
    scRNA_fastq_folders = recursive_fastq_folder_search(argv.scRNA_fastq_folder)
    mcRNA_fastq_folders = recursive_fastq_folder_search(argv.mcRNA_fastq_folder)
    # make a seperate folder inside the output folder for both sc and mcrna
    output_scrna = argv.output+"scRNA/"
    output_rna = argv.output+"RNA/"
    output_rcompare = argv.output+"r_compare/"
    scrna_exists = mkdir_ifnotexists(output_scrna)
    mcrna_exists = mkdir_ifnotexists(output_rna)

    # delete everything if this multiple run is already present, we don't want folders with stuff already in it.
    if mcrna_exists:
        shutil.rmtree(output_rna, ignore_errors=True)
        mkdir_ifnotexists(output_rna)
    if scrna_exists:
        shutil.rmtree(output_scrna, ignore_errors=True)
        mkdir_ifnotexists(output_scrna)

    try:
        scRNA_outputs = run_all_pipelines(scRNA_fastq_folders, output_scrna, argv.config)
    except:
        sys.exit("Something went wrong when running pipelines for scrna")

    try:
        mcRNA_outputs = run_all_pipelines(mcRNA_fastq_folders, output_rna, argv.config)
    except:
        sys.exit("Something went wrong when running pipelines for mcrna")


    # after running every pipeline, make a comparison between each scRNA and mcRNA run, but only when their samples
    # match.
    try:
        scan_pipelines(scRNA_outputs, mcRNA_outputs, output_rcompare)
    except:
        sys.exit("Something went wrong when scanning pipelines with R")
    # sort all the scans
    try:
        sort_pipeline_scans(output_rcompare)
    except:
        sys.exit("Something went wrong when sorting pipeline scans.")

    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main(argv=input_parsing()))
