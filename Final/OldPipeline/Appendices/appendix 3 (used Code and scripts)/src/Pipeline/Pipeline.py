#!/usr/bin/env python3
"""
This python program is meant for running an RNA gene quantification pipeline. It can run with any RNA-seq data if given
in fastq format.

Example usage;
python3 Pipeline.py --aligner Hisat2[../Indeces/mm10/hisat2/genome, p=7] --rna_quantifier Homer[condenseGenes=True,
normilization=raw] --config config.json -t -o ../myoutputdir -f ../myfastqdir

DO NOT GIVE THE SEPERATE TOOLS ANY INPUT/OUTPUT PARAMETERS, THE PIPELINE WILL HANDLE THIS.

Options to define with the pipeline (standards are defined, for further explanation on these arguments, please
refer to the manual of said tool.);

### FastQC ###
## Url for manual; https://www.bioinformatics.babraham.ac.uk/projects/fastqc/
## Url for cli-manual (not direct source); http://manpages.ubuntu.com/manpages/xenial/man1/fastqc.1.html
casava=False, nofilter=False, extract=False, java=None, noextract=False, nogroup=False, format=None, threads=1,
contaminants=None, adapters=None, limits=None, kmers=None, quiet=False, dir=None

### AfterQC ###
## Url for manual; https://github.com/OpenGene/AfterQC
# correction_threshold is the percentage of reads that are allowed to be bad. e.g. 5 for 5% of the reads that are
# allowed to be bad.
correction_threshold, trim_front=-1, trim_tail=-1, qualified_quality_phred=20, unqualified_base_limit=0,
poly_size_limit=35, allow_mismatch_in_poly=5, n_base_limit=5, seq_len_req=35, qc_sample=1000000, qc_kmer=8

### Hisat2 ###
## Url for manual; https://ccb.jhu.edu/software/hisat2/manual.shtml
hisat2_index, skip=None, upto=None, trim5=0, trim3=0, n_ceil="L,0,0.15", ignore_quals=False, nofw=False, norc=False,
pen_cansplice=0, pen_noncansplice=12, pen_canintronlen="G,-8,1", pen_noncanintronlen="G,-8,1", min_intronlen=20,
max_intronlen=500000, no_temp_splicesite=False, no_spliced_alignment=False, rna_strandness=None, tmo=False, dta=False,
dta_cufflinks=False, add_chrname=False, p=1, ma=False, end_to_end=False, local=False, mp="6,2", sp="2,1",
no_softclip=False, np=1, rdg="5,3", rfg="5,3", score_min="L,0.0,-0.2"

### STAR ###
## Url for manual; https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf
# star is very picky with it's index, be sure to test star manually first. not all reads are guaranteed to work with
# this aligner
genomeDir, outFilterType=False, outFilterMultimapNmax=20, alignSJoverhangMin=8, alignSJDBoverhangMin=1,
outFilterMismatchNmax=999, outFilterMismatchNoverReadLmax=0.04, alignIntronMin=20, alignIntronMax=1000000,
alignMatesGapMax=1000000, runThreadN=1

### bwa (mem) ###
## Url for manual; http://bio-bwa.sourceforge.net/bwa.shtml
# for bwa we used the bwa-mem aligner
dbprefix, t=1 , k=19, w=100, d=100, r=1.5, c=500, D=0.5, m=50, W=0, A=1, B=4, O=6, E=1, L=5, U=17

### Analyze_repeats (homer) ###
## Url for manual; http://homer.ucsd.edu/homer/ngs/analyzeRNA.html
# this only runs the analyse repeats script for homer. (and makeTagDir because this is a requirement for the script.)
input_file="rna", genome="mm10", maxdiv=1.0, mindiv=0, minLength=0, maxLength=False, minLengthP=0, maxLengthP=100,
noexon=False, rmsk=False, three_p=False, five_p=False, og=False, count="3utr", strand="both", pc=0, log=False,
sqrt=False, condenseGenes=False, noCondensing=False, noCondensingParts=False, min=False, normilization="norm 1e7",
quantile=False
"""
__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"
# imports
import sys
import argparse
import time
import os
import json
from Scmc_RNA_Pipeline_tools import QC
from Scmc_RNA_Pipeline_tools import alignment
from Scmc_RNA_Pipeline_tools import RNA_quantification

# constants
pipeline_start_time_epoch = time.time()
pipeline_start_time = time.strftime('Pipeline_%Y_%m_%d_%H.%M.%S_YMDHMS', time.localtime(pipeline_start_time_epoch))
qc_suggestions = ["afterqc", "fastqc"]
alignment_suggestions = ["hisat2", "star", "bwa"]
rna_quantification_suggestions = ["homer"]
LOG_FILE = sys.stdout
CONFIGURATION = dict()

# classes
# functions

def input_parsing():
    """
    Parse command line input.
    """
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-q", "--quality_control", type=test_qc, required=False,
                        help="Give the quality control tool you want to use. Together with the tool's parameters.")
    parser.add_argument('-a', '--aligner', type=test_aligner, required=False,
                        help="Give the alignment tool you want to use. Together with the tool's parameters.")
    parser.add_argument('-r', "--rna_quantifier", type=test_rna_quantification, required=False,
                        help="Give the RNA quantification tool you want to use. Together with the tool's parameters.")
    parser.add_argument('-f', "--fastq_folder", type=test_fastq, required=False,
                        help="Give the folder/directory with your desired fastq's you want to put through the "
                             "pipeline.")
    parser.add_argument('-s', "--sam_folder", type=test_sam, required=False,
                        help="If the alignment step is not set, Give the folder/directory with your desired sam files "
                             "you want to put through the pipeline.")
    parser.add_argument('-o', '--output', type=test_output, required=True,
                        help="Give the output directory the pipeline will use while Processing all in-between data.")
    parser.add_argument('-c', '--config', type=test_config, required=True)
    parser.add_argument('-l', '--log', action="store_true")

    args = parser.parse_args()
    test_args_combinations(parser, args)
    parse_config_file(parser, args)
    return args


def test_args_combinations(parser, args):
    """
    Test if the combination of arguments is valid
    """
    if args.quality_control and (args.aligner is None and args.rna_quantifier):
        parser.error("Omitting in between step --aligner is not allowed!")
    if args.aligner or args.quality_control:
        if not args.fastq_folder:
            parser.error("Without a directory with your desired fastq's, "
                         "no alignment or quality control can be done!")
        if args.sam_folder:
            parser.error("RNA quantification uses the input directory made by running an alignment, "
                         "--sam_folder is ignored!")
    if args.rna_quantifier and args.aligner is None:
        if not args.sam_folder:
            parser.error("Without a directory with your desired sam files, no rna quantification can be done!")
        if args.fastq_folder:
            parser.error("Alignment step is omitted, --fastq_folder is ignored!")
    return 0


def assess_concurrence(parser, args):
    """
    Assess if every tool is assigned an executable
    """
    if args.quality_control:
        has_value(parser, args.quality_control, "executable")
    if args.aligner:
        has_value(parser, args.aligner, "executable")
        has_value(parser, args.aligner, "index")
    if args.rna_quantifier:
        has_value(parser, args.rna_quantifier, "executable")
        if args.rna_quantifier.split("[")[0].lower() == "homer":
            has_value(parser, args.rna_quantifier, "directory")


def has_value(parser, tool_argument, key):
    toolname = tool_argument.split("[")[0].lower()
    if toolname in CONFIGURATION and key in CONFIGURATION[toolname]:
        pass
        # leafs = has_unrunnable_dict_leafs(CONFIGURATION[toolname]):
        # if leafs:
        #   parser.error("\"{}\" in JSON config file should be an executable".format(leafs))
    else:
        parser.error("Tool \"{}\" doesn't have \"{}\" in the JSON config file".format(toolname, key))
    return


def parse_config_file(parser, args):
    """
    Assigns dict to global CONFIGURATION given the json config file.
    """
    global CONFIGURATION
    with open(args.config) as json_config_file:

        try:
            CONFIGURATION = json.load(json_config_file)
        except ValueError as e:
            parser.error("Config file \"{}\" isn't in JSON format.".format(args.config))

    assess_concurrence(parser, args)
    return


def test_config(config):
    """
    Test if config is json file, exists and is readable
    """
    if not config.endswith(".json"):
        raise argparse.ArgumentTypeError("File \"{}\" should be a JSON file with .json extension".format(config))
    if not (os.path.isfile(config) and os.access(config, os.R_OK)):
        raise argparse.ArgumentTypeError("JSON file \"{}\" doesn't exist or isn't readable.".format(config))
    return config


def test_qc(qc_suggestion):
    """
    Test QC argparse input
    """
    if not qc_suggestion.split("[")[0].lower() in qc_suggestions:
        raise argparse.ArgumentTypeError("Option \"{}\" is not available as a quality control option. Available options"
                                         " are: ".format(qc_suggestion.lower()) + ", ".join(qc_suggestions))
    qc_suggestion_new = [qc_suggestion.split("[")[0].lower()]
    qc_suggestion_new.extend(qc_suggestion.split("[")[1:])
    return "[".join(qc_suggestion_new)


def test_aligner(aligner_suggestion):
    """
    Test aligner argparse input
    """
    if not aligner_suggestion.split("[")[0].lower() in alignment_suggestions:
        raise argparse.ArgumentTypeError("Option \"{}\" is not available as an aligner option. Available options are: "
                                         .format(aligner_suggestion.lower()) + ", ".join(alignment_suggestions))
    aligner_suggestion_new = [aligner_suggestion.split("[")[0].lower()]
    aligner_suggestion_new.extend(aligner_suggestion.split("[")[1:])
    return "[".join(aligner_suggestion_new)


def test_rna_quantification(rna_quantifier):
    """
    Test Rma quantification argparse input
    """
    if not rna_quantifier.split("[")[0].lower() in rna_quantification_suggestions:
        raise argparse.ArgumentTypeError("Option \"{}\" is not available as an rna quantification option. Available"
                                         " options are: ".format(rna_quantifier.lower()) +
                                         ", ".join(rna_quantification_suggestions))
    rna_quantifier_new = [rna_quantifier.split("[")[0].lower()]
    rna_quantifier_new.extend(rna_quantifier.split("[")[1:])
    return "[".join(rna_quantifier_new)


def test_fastq(fastq_dir):
    """
    Test fastq directory argparse input
    """
    if not os.path.isdir(fastq_dir):
        raise argparse.ArgumentTypeError("Directory \"{}\" is not an existing directory, please input a valid one."
                                         .format(fastq_dir))
    # check for existence of fastq files
    if not check_if_exists_fastq(fastq_dir):
        raise argparse.ArgumentTypeError("Fastq directory \"{}\" does not contain any valid fastq files!"
                                         .format(fastq_dir))
    return fastq_dir


def test_sam(sam_dir):
    """
    Test sam directory argparse input
    """
    if not os.path.isdir(sam_dir):
        raise argparse.ArgumentTypeError("Directory \"{}\" is not an existing directory, please input a valid one."
                                         .format(sam_dir))
    return sam_dir


def test_output(output_dir):
    """
    Test output folder argparse input
    """
    if not os.path.isdir(output_dir):
        raise argparse.ArgumentTypeError("Output folder \"{}\" is invalid or does not exist, please input a valid "
                                         "output folder.".format(output_dir))
    return output_dir + "/"


def check_if_exists_fastq(fastq_dir):
    """
    Function that returns whether or not a dir has fastq files in them.
    """
    contains_fastq = False
    for item in os.listdir(fastq_dir):
        if os.path.isfile(fastq_dir + "/" + item) and (item.endswith(".fastq") or item.endswith(".fq")):
            contains_fastq = True
            break
    return contains_fastq


def make_output_folders(argv):
    """
    Processes making of all the needed output directories for all the different tools.
    returns a dictionary of output dirs for further use. (See give_output_folders function.)
    """
    # first make root dir for the pipeline.
    os.mkdir(argv.output)
    # then continue to make preperations for all the other steps.
    os.mkdir(argv.output + "Quality_control")
    os.mkdir(argv.output + "Alignment")
    os.mkdir(argv.output + "RNA_quantification")
    tools_dirs = give_tool_output_folders(argv)
    for directory in tools_dirs.values():
        os.mkdir(directory)
    return tools_dirs


def give_tool_output_folders(argv):
    """
    Returns a dictionary of all the output folders in the main pipeline folder.
    These are used to make the seperate tools properly do their I/O
    """
    tool_dirs = dict()
    if argv.quality_control:
        qc_tool = argv.quality_control.split("[")[0].lower()
        tool_dirs["QC"] = argv.output + "Quality_control/" + qc_tool
    if argv.aligner:
        align_tool = argv.aligner.split("[")[0].lower()
        tool_dirs["aligner"] = argv.output + "Alignment/" + align_tool
    if argv.rna_quantifier:
        rna_tool = argv.rna_quantifier.split("[")[0].lower()
        tool_dirs["rna"] = argv.output + "RNA_quantification/" + rna_tool
    return tool_dirs


def parse_tool_parameters(tool_param):
    """
    Parse command line input for each tool so that these can just be thrown in the function later on.
    Argparse can't handle this, so that's why there's a need for this function.
    """
    tool_params = [i.split(",") for i in tool_param.split("[")[1].split("]")[:-1][0].split(", ")]
    tool_param_args = list()
    tool_param_kwargs = dict()
    for i in tool_params:
        for j in i:
            j = j.strip()
            if "=" in j:
                # unpack key args into dict
                j = j.split("=")
                param_key = j[0]
                param_value = j[1]
                param_value = type_correction(param_value)
                # check if param_value needs to be a certain type.
                tool_param_kwargs[param_key] = param_value
            else:
                # just append the normal args sequentually.
                j = type_correction(j)
                tool_param_args.append(j)
    return tool_param_args, tool_param_kwargs


def run_qc_step(argv, qc_output_dir):
    """
    Run the quality control, exit if there is faulty fastq files. Exit if parms contain I/O options.
    """
    qc_tool = argv.quality_control.split("[")[0].lower()
    qc_input_dir = argv.fastq_folder
    qc_args, qc_kwargs = parse_tool_parameters(argv.quality_control)

    # before running any of the possible tools, change dir to the output for good measure.
    os.chdir(qc_output_dir)
    if qc_tool == "afterqc":
        # exit if I/O options exist.
        check_io(qc_args, 1, qc_tool)
        # insert the proper I/O args.
        qc_args.insert(1, qc_input_dir)
        qc_args.insert(2, qc_output_dir)
        # run the tool
        qualifier = QC.After_qc(LOG_FILE, CONFIGURATION[qc_tool]["executable"])
        did_correction = qualifier.after_qc(*qc_args, **qc_kwargs)
        if did_correction:
            sys.exit("AfterQC ran a correction on your data because it contained flaws, please take a look at the "
                     "new corrected fasta files and reselect your input.")
    elif qc_tool == "fastqc":
        # exit if I/O options exist.
        check_io(qc_args, 1, qc_tool)
        # insert the proper I/O args.
        qc_args.insert(0, qc_input_dir)
        qc_args.insert(0, qc_output_dir)
        # run the tool
        qualifier = QC.FastQC(LOG_FILE, CONFIGURATION[qc_tool]["executable"])
        did_correction = qualifier.fastQC(*qc_args, **qc_kwargs)
    return 0


def run_align_step(argv, tool_dirs):
    """
    Run the alignment, exit if there is an I/O problem.
    """
    align_tool = argv.aligner.split("[")[0].lower()
    align_input_dir = argv.fastq_folder
    align_output_dir = tool_dirs["aligner"]
    align_args, align_kwargs = parse_tool_parameters(argv.aligner)
    configuration = CONFIGURATION[align_tool]

    # before running any of the possible tools, change dir to the output for good measure.
    os.chdir(align_output_dir)
    if align_tool == "hisat2":
        # exit if I/O options exist.
        check_io(align_args, 1, align_tool)
        # insert the proper I/O args.
        align_args.insert(1, align_input_dir)
        align_args.insert(2, align_output_dir)
        # run the tool
        aligner = alignment.Hisat2(LOG_FILE, configuration["executable"], configuration["index"])
        aligner.hisat2(*align_args, **align_kwargs)
    elif align_tool == "star":
        # exit if I/O options exist.
        check_io(align_args, 1, align_tool)
        # insert the proper I/O args.
        align_args.insert(1, align_input_dir)
        align_args.insert(2, align_output_dir)
        # run the tool
        aligner = alignment.Star(LOG_FILE, configuration["executable"], configuration["index"])
        aligner.star(*align_args, **align_kwargs)
    elif align_tool == "bwa":
        # exit if I/O options exist.
        check_io(align_args, 1, align_tool)
        # insert the proper I/O args.
        align_args.insert(1, align_input_dir)
        align_args.insert(2, align_output_dir)
        # run the tool
        aligner = alignment.Bwa(LOG_FILE, configuration["executable"], configuration["index"])
        aligner.bwa(*align_args, **align_kwargs)
    return 0


def run_rna_quantification_step(argv, tool_dirs):
    """
    Run quantification, exit if there is an I/O problem.
    """
    rna_tool = argv.rna_quantifier.split("[")[0].lower()
    rna_input_dir = tool_dirs["aligner"]
    rna_output_dir = tool_dirs["rna"]
    rna_args, rna_kwargs = parse_tool_parameters(argv.rna_quantifier)

    # before running any of the possible tools, change dir to the output for good measure.
    os.chdir(rna_output_dir)
    if rna_tool == "homer":
        # exit if I/O options exist."./Pipeline.py
        check_io(rna_args, 0, rna_tool)
        # insert the proper I/O args.
        rna_args.insert(0, rna_input_dir)  # changed to get all sam files in dir
        rna_args.insert(1, rna_output_dir)
        # run the tool
        quantifier = RNA_quantification.Analyze_repeats_homer(LOG_FILE, CONFIGURATION[rna_tool]["executable"],
                                                              CONFIGURATION[rna_tool]["directory"])
        quantifier.analyze_repeats_homer(*rna_args, **rna_kwargs)

    return 0


def check_io(tool_args, proper_size, tool_name):
    """
    Checks if IO input is not present, if it is, exit.
    """
    if len(tool_args) > proper_size:
        sys.exit("{} doesn't need Input Output options when being run in the pipeline! These get automatically"
                 " generated... Please run the pipeline again without I/O options in this specific tool."
                 .format(tool_name))
    return 0


def type_correction(a_thing):
    """
    Try convert string to proper type. If not possible, just return string.
    """
    if a_thing == "True":
        a_thing = True
    elif a_thing == "False":
        a_thing = False
    elif a_thing == "None":
        a_thing = None
    else:
        try:
            a_thing = float(a_thing)
            if a_thing.is_integer():
                a_thing = int(a_thing)
        except ValueError:
            pass
    return a_thing


def start_pipeline(argv, tool_dirs):
    """
    Run steps of the pipeline
    """

    # run each step in turn.
    if argv.quality_control:
        run_qc_step(argv, tool_dirs["QC"])
    if argv.aligner:
        run_align_step(argv, tool_dirs)
    if argv.rna_quantifier:
        if argv.sam_folder:
            tool_dirs["aligner"] = argv.sam_folder
        run_rna_quantification_step(argv, tool_dirs)
    return


def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    # parse arguments
    raw_arguments = argv
    argv = input_parsing()
    # prepare for work being done by making correct dirs and checking if everything is in it's place.
    # if nothing went wrong, make output directory.
    # define the output dir first.
    argv.output = argv.output + "/" + pipeline_start_time + "/"
    tool_dirs = make_output_folders(argv)
    global LOG_FILE
    if argv.log:
        LOG_FILE = argv.output + "log.txt"
    else:
        LOG_FILE = False
    # log starting the pipeline
    pipeline_started_preformat = "Started pipeline at: {} with the following arguments:{}{}"
    start_log = pipeline_started_preformat.format(pipeline_start_time, os.linesep, " ".join(raw_arguments))
    print(start_log)
    if LOG_FILE:
        with open(LOG_FILE, "a") as open_log_file:
            print(start_log, file=open_log_file)

    # run the steps of the pipeline
    start_pipeline(argv, tool_dirs)

    final_log = "Took {:0.2f} Seconds to run pipeline.".format(time.time() - pipeline_start_time_epoch)
    print(final_log)
    if LOG_FILE:
        with open(LOG_FILE, "a") as open_log_file:
            print(final_log, file=open_log_file)
    return argv.output


# initiate
if __name__ == "__main__":
    sys.exit(main())
