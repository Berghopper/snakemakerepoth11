#!/usr/bin/env python3
"""
This subpackage is meant for handling the mapping of our pipeline.
It contains several loose functions which run the desired tool.
"""
__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"

# imports
import sys
import os
import subprocess
from Scmc_RNA_Pipeline_tools.pipeline_tool import PipelineTool
# constants
# classes

class Hisat2(PipelineTool):
    def __init__(self, log_file, executable, index):
        super(Hisat2, self).__init__(log_file, executable)
        self.log_hisat2_version()
        self.index = index

    def hisat2(self, unpaired_reads_dir, sam_output_dir, skip=None, upto=None, trim5=0, trim3=0,
               n_ceil="L,0,0.15", ignore_quals=False, nofw=False, norc=False, pen_cansplice=0,
               pen_noncansplice=12, pen_canintronlen="G,-8,1", pen_noncanintronlen="G,-8,1", min_intronlen=20,
               max_intronlen=500000, no_temp_splicesite=False, no_spliced_alignment=False,
               rna_strandness=None, tmo=False, dta=False, dta_cufflinks=False, add_chrname=False,
               p=1, ma=False, end_to_end=False, local=False, mp="6,2", sp="2,1", no_softclip=False,
               np=1, rdg="5,3", rfg="5,3", score_min="L,0.0,-0.2"):
        '''
        excecutes hisat2 mapping / alignment command given the following positional arguments:
        hisat2_index: the name of the index in a directory
        unpaired_reads_dir: a directory where unpaired reads are stored (only the given directory is checked)
        sam_output: a sam file for output
    
        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        '''
        
        extensions = ("fastq", "fastq.gz", "fq", "fq.gz")
        
        read_files = os.listdir(unpaired_reads_dir)
        unpaired_reads_comma_separated = [unpaired_reads_dir+"/"+f for f in read_files if f.split('.')[-1] in extensions]
    
        sam_output = sam_output_dir+"/output.sam"

        options = [" --ignore-quals " if ignore_quals else "",
                   " --nofw " if nofw else "",
                   " --norc " if norc else "",
                   " --no-temp-splicesite " if no_temp_splicesite else "",
                   " --no-spliced-alignment " if no_spliced_alignment else "",
                   " --tmo " if tmo else "",
                   " --dta " if dta else "",
                   " --dta-cufflinks " if dta_cufflinks else "",
                   " --add_chrname " if add_chrname else "",
                   " --skip "+str(skip) if skip else "",
                   " --upto "+str(upto) if upto else "",
                   " --ma "+str(ma) if ma else "",
                   " --end-to-end " if end_to_end else "",
                   " --local " if local else "",
                   " --no-softclip " if no_softclip else "",
                   " --rna-strandness "+str(rna_strandness) if rna_strandness else ""]

        exec_string = (self.executable + " -p "+str(p)
                       +" -x "+self.index+" -U "+str(",".join(unpaired_reads_comma_separated))+" --trim5 "+str(trim5)
                       +" --trim3 " + str(trim3) +" --n-ceil " + str(n_ceil) +" --pen-cansplice " + str(pen_cansplice)
                       +" --pen-noncansplice " + str(pen_noncansplice) +" --pen-canintronlen " + str(pen_canintronlen)
                       +" --pen-noncanintronlen " + str(pen_noncanintronlen)
                       +" --min-intronlen " + str(min_intronlen) +" --max-intronlen " + str(max_intronlen)
                       +" --mp " + str(mp) +" --sp " + str(sp) +" --np " + str(np) +" --rdg " + str(rdg) +" --rfg "
                       +str(rfg)+" --score-min " + str(score_min) +" -S " + sam_output + "".join(options))

        log = "Executing hisat2 with following command; "+exec_string
        print(log)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(log, file=open_log_file)
        
        os.system(exec_string)
        return


    def log_hisat2_version(self):
        '''
        logs version for hisat2
        '''
        version = str(subprocess.check_output("hisat2 --version", shell=True)).split("\\n")[0][2:]
        print(version)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version, file=open_log_file)
        return


class Star(PipelineTool):
    def __init__(self, log_file, executable, index):
        super(Star, self).__init__(log_file, executable)
        self.log_star_version()
        self.index = index

    def star(self, readFilesIn, outFileNamePrefix,
             outFilterType=False, outFilterMultimapNmax=20, alignSJoverhangMin=8, alignSJDBoverhangMin=1,
             outFilterMismatchNmax=999, outFilterMismatchNoverReadLmax=0.04, alignIntronMin=20,
             alignIntronMax=1000000, alignMatesGapMax=1000000, runThreadN=1):
        '''
        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        '''

        extensions = ("fastq", "fastq.gz", "fq", "fq.gz")

        read_files = os.listdir(readFilesIn)
        readFilesIn = [readFilesIn + "/" + f for f in read_files if f.split('.')[-1] in extensions]

        if outFilterType:
            outFilterType = " --outFilterType "
        else:
            outFilterType = ""

        for i, read in enumerate(readFilesIn):
            exec_string = (self.executable + " --runThreadN " + str(runThreadN) + " --genomeDir " + self.index
                           + " --readFilesIn " + read + " --outFileNamePrefix "
                           + outFileNamePrefix + "/output" + str(i) + " " + outFilterType
                           + " --outFilterMultimapNmax " + str(outFilterMultimapNmax)
                           + " --alignSJoverhangMin " + str(alignSJoverhangMin)
                           + " --alignSJDBoverhangMin " + str(alignSJDBoverhangMin)
                           + " --outFilterMismatchNmax " + str(outFilterMismatchNmax)
                           + " --outFilterMismatchNoverReadLmax " + str(outFilterMismatchNoverReadLmax)
                           + " --alignIntronMin " + str(alignIntronMin) + " --alignIntronMax " + str(alignIntronMax)
                           + " --alignMatesGapMax " + str(alignMatesGapMax))

            log = "Executing rna-star with following command; " + exec_string
            print(log)
            if self.log_file:
                with open(self.log_file, "a") as open_log_file:
                    print(log, file=open_log_file)

            os.system(exec_string)
        return


    def log_star_version(self):
        """
        logs version for star
        """
        version = str(subprocess.check_output("STAR --version", shell=True)).split("\\n")[0][2:]
        print(version)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version, file=open_log_file)
        return


class Bwa(PipelineTool):
    def __init__(self, log_file, executable, index):
        super(Bwa, self).__init__(log_file, executable)
        self.log_bwa_version()
        self.index = index

    def bwa(self, reads, outfile, t=1 , k=19, w=100, d=100, r=1.5, c=500, D=0.5, m=50, W=0, A=1, B=4, O=6,
            E=1, L=5, U=17):
        '''
        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        '''
        extensions = ("fastq", "fastq.gz", "fq", "fq.gz")

        read_files = os.listdir(reads)
        reads = [reads + "/" + f for f in read_files if
                                          f.split('.')[-1] in extensions]

        # BWA only expects one read.
        # for every read: execute an alignment
        for i, read in enumerate(reads):
            exec_string = (self.executable + " mem -t " + str(t) + " -k " + str(k) + " -w " + str(w) + " -d " + str(d)
                           + " -r " + str(r) + " -c " + str(c) + " -D " + str(D) + " -m " + str(m) + " -W " + str(W)
                           + " -A " + str(A) + " -B " + str(B) + " -O " + str(O) + " -E " + str(E) + " -L " + str(L)
                           + " -U " + str(U) + " " + self.index + " " + read + " > " + outfile + "/output" + str(i) + ".sam")

            log = "Executing bwa with following command; " + exec_string
            print(log)
            if self.log_file:
                with open(self.log_file, "a") as open_log_file:
                    print(log, file=open_log_file)

            os.system(exec_string)
        return

    def log_bwa_version(self):
        """
        logs version for star
        """
        # couldn't find version check option for bwa
        output = subprocess.Popen("bwa", universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        version = "bwa" + str(output.communicate()[1].split("\n")[2])
        print(version)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version, file=open_log_file)
        return


# functions


def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    return 0

# initiate
if __name__ == "__main__":
    sys.exit(main())
