#!/usr/bin/env python3
"""
This subpackage is meant for handling the RNA quantification / normalisation of our pipeline.
It contains several loose functions which run the desired tool.
"""

__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"
# imports
import sys
import os
import subprocess
from Scmc_RNA_Pipeline_tools.pipeline_tool import PipelineTool

# constants
# classes
# functions


class Analyze_repeats_homer(PipelineTool):
    def __init__(self, log_file, executable, directory):
        super(Analyze_repeats_homer, self).__init__(log_file, executable)
        self.analyzeRepeatsExecutable = executable["analyzeRepeatsExecutable"]
        self.makeTagDirectoryExecutable = executable["makeTagDirectoryExecutable"]
        self.directory = directory

        self.log_analyze_repeats_homer_version()

    def analyze_repeats_homer(self, sam_files, output_folder, input_file="rna", genome="mm10", maxdiv=1.0, mindiv=0,
                              minLength=0,
                              maxLength=False, minLengthP=0, maxLengthP=100, noexon=False, rmsk=False, three_p=False,
                              five_p=False, og=False, count="3utr", strand="both", pc=0, log=False, sqrt=False,
                              condenseGenes=False, noCondensing=False, noCondensingParts=False, min=False,
                              normilization="norm 1e7", quantile=False):
        """
        Not all parameters have been included because of confusion of what they do. The above documentation has been copied
        from the homer help of their analyzeRepeats.pl perl script.

        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        """

        # first change directory to homer bin (else it can't find other scripts it depends on, mumble mumble...)
        old_cwdir = os.getcwd()
        os.chdir(self.directory + "/bin")
        # first make lists with all the correct options and check the input.
        extensions = ("sam")

        sams = os.listdir(sam_files)
        sam_files = " ".join([sam_files + "/" + f for f in sams if f.split('.')[-1] in extensions])
        input_files = ["rna", "repeats"]
        homer_genomes = os.listdir(self.directory+"/data/genomes")
        counts = ["genes", "exons", "introns", "5utr", "3utr", "cds"]
        strands = ["+", "-", "both"]
        normilizations = ["rpkm", "fpkm", "norm", "normMatrix", "noadj", "raw"]
        # check inputs.
        if not input_file in input_files:
            sys.exit("Wrong input file defined, can either be \"rna\" or \"repeats\".")
        if not genome in homer_genomes:
            sys.exit("Homer genome not found, make sure you have the right genomes installed!")
        if not count in counts:
            sys.exit("Invalid count option, valid count options are: "+", ".join(counts))
        if not strand in strands:
            sys.exit("Invalid strand option, available strand options are: "+", ".join(strands))
        if not normilization.split(" ")[0] in normilizations:
            sys.exit("Invalid normilization option, available normilization options are: "+", ".join(normilizations))
        # define boolean inputs (optional arguments)
        if not maxLength:
            maxLength = ""
        else:
            maxLength = "-maxLength " + str(maxLength)
        if not noexon:
            noexon = ""
        else:
            noexon = "-noexon"
        if not rmsk:
            rmsk = ""
        else:
            rmsk = "-rmsk"
        if not three_p:
            three_p = ""
        else:
            three_p = "-3p"
        if not five_p:
            five_p = ""
        else:
            five_p = "-5p"
        if not og:
            og = ""
        else:
            og = "-og"
        if not log:
            log = ""
        else:
            log = "-log"
        if not sqrt:
            sqrt = ""
        else:
            sqrt = "-sqrt"
        if not condenseGenes:
            condenseGenes = ""
        else:
            condenseGenes = "-condenseGenes"
        if not noCondensing:
            noCondensing = ""
        else:
            noCondensing = "-noCondensing"
        if not noCondensingParts:
            noCondensingParts = ""
        else:
            noCondensingParts = "-noCondensingParts"
        if not min:
            min = ""
        else:
            min = "-min "+ str(min)
        if not quantile:
            quantile = ""
        else:
            quantile = "-quantile"


        # first make outputfolder for making tagdir (Homer needs to index some stuff)
        tagdir = output_folder + "/HOMERTEMPTAGDIR"
        # first make a needed tag directory.
        exec_string = self.makeTagDirectoryExecutable + " "+tagdir+" "+sam_files
        print("Making homer tag directory using following command (this might take a while); "+exec_string)
        os.system(exec_string)
        # Run analyserepeats.pl
        exec_string = self.analyzeRepeatsExecutable +" "+input_file+" "+genome+ " -d "+tagdir+" -maxdiv "+\
                      str(maxdiv)+" -mindiv "+str(mindiv)+" -minLength "+str(minLength)+" "+maxLength+" -minLengthP "+\
                      str(minLengthP)+" -maxLengthP "+str(maxLengthP)+" "+noexon+" "+rmsk+" "+three_p+" "+five_p+" "+\
                      og+" -count "+count+" -strand "+strand+" -pc "+str(pc)+" "+log+" "+sqrt+" "+condenseGenes+" "+\
                      noCondensing+" "+noCondensingParts+" "+min+" -"+normilization+" "+quantile+" > "+\
                      output_folder+"/RNA_quantification_`date '+%Y_%m_%d__%H_%M_%S'`.txt"
        log = "Executing analyzeRepeats.pl with following command; " + exec_string
        print(log)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(log, file=open_log_file)
        os.system(exec_string)
        # go back to old directory, seriously, wtf homer.
        os.chdir(old_cwdir)

    def log_analyze_repeats_homer_version(self):
        '''
        logs version for after_qc
        '''
        version_software = str(subprocess.check_output("cat "+self.directory+"/config.txt",
                                    shell=True)).split("\\n")[9].split("\\t")[1]
        print(version_software)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version_software, file=open_log_file)
        return


def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())