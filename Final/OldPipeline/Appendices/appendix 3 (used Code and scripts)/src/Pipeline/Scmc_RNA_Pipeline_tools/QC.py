#!/usr/bin/env python3
"""
This subpackage is meant for handling the quality control of our pipeline.
It contains several loose functions which run the desired tool.
"""
__author__ = "Casper Peters & Robert Warmerdam (Berghopper & ca_warmerdam)"
__credits__ = ["Casper Peters", "Robert Warmerdam"]
__maintainer__ = "de hakketakkers (Bitbucket team)"
__status__ = "Prototype"

# imports
import sys
import os
import json
import subprocess
from Scmc_RNA_Pipeline_tools.pipeline_tool import PipelineTool

# constants
# classes


class After_qc(PipelineTool):
    def __init__(self, log_file, executable):
        super(After_qc, self).__init__(log_file, executable)
        self.log_after_qc_version()

    def after_qc(self, correction_threshold, input_dir, output_dir, trim_front=-1, trim_tail=-1,
                 qualified_quality_phred=20, unqualified_base_limit=0, poly_size_limit=35, allow_mismatch_in_poly=5,
                 n_base_limit=5, seq_len_req=35, qc_sample=1000000, qc_kmer=8
                 ):
        """
        Used for running after_qc, this is only for single-end
        correction_threshold is the percentage of max bad reads, if this is reached or bigger than, read will be trimmed.
        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        """
        did_correction = False
        # make bad and good dir
        good_dir, bad_dir = output_dir+"/"+"good", output_dir+"/"+"bad"
        old_wd = os.getcwd()
        # change to directory to input dir for renaming
        os.chdir(input_dir)
        # First rename all files so they have R1_ in front of it. (After_qc needs this sadly...)
        for item in os.listdir(input_dir):
            os.rename(item, "R1_" + item)
        # change to directory to output dir for running the tool
        os.chdir(output_dir)
        # Then run AfterQC with only quality control.
        exec_string = self.executable + " --qc_only --input_dir="+input_dir+\
                      " --qc_sample="+str(qc_sample)+" --qc_kmer="+str(qc_kmer)
        log = "Executing AfterQC with following command; " + exec_string
        print(log)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(log, file=open_log_file)
        os.system(exec_string)
        # Check if the correct_percentage is met, if not, run the actual afterqc and do a quality check again. (per file!)
        # check if list is empty, if so, just leave everything be.
        bad_reads = self.after_qc_json_checker(float(correction_threshold), input_dir, output_dir)
        if not bad_reads:
            print("No bad reads found, I guess you're lucky! (Starting alignment)")
        if bad_reads:
            did_correction = True
            for bad_read in bad_reads:
                exec_string = self.executable + " --read1_file="+bad_read+\
                              " --qc_sample="+str(qc_sample)+" --qc_kmer="+str(qc_kmer)+" --good_output_folder="+good_dir+\
                              " --bad_output_folder="+bad_dir+" --trim_front="+str(trim_front)+" --trim_tail="+\
                              str(trim_tail)+" --qualified_quality_phred="+str(qualified_quality_phred)+\
                              " --unqualified_base_limit="+str(unqualified_base_limit)+" --poly_size_limit="+\
                              str(poly_size_limit)+" --allow_mismatch_in_poly="+str(allow_mismatch_in_poly)+\
                              " --n_base_limit="+str(n_base_limit)+" --seq_len_req="+str(seq_len_req)
                log = "Trying to fix read: "+bad_read.split("_")[1].split("/")[-1]\
                      +" Executing AfterQC with following command; "+exec_string
                print(log)
                if self.log_file:
                    with open(self.log_file, "a") as open_log_file:
                        print(log, file=open_log_file)
                os.system(exec_string)
        # change to directory to input dir for renaming
        os.chdir(input_dir)
        # Rename files to original
        for item in os.listdir(input_dir):
            os.rename(item, "".join(item.split("_")[1:]))
        os.chdir(old_wd)
        return did_correction

    def after_qc_json_checker(self, correction_threshold, input_dir, output_dir):
        """
        function that checks quality per file, if the read is bad, the name gets appened to a list that
        gets returned in the end. after_qc needs this function, this is NOT a seperate tool!
        """
        bad_reads = []
        old_wd = os.getcwd()
        os.chdir(os.getcwd()+"/QC")
        for data_file in os.listdir(output_dir+"/QC"):
            if data_file.endswith(".json"):
                json_data = json.load(open(data_file))
                if (correction_threshold/100) <= (json_data['afterqc_main_summary']["bad_reads"]/
                                                      json_data['afterqc_main_summary']["total_reads"]):
                    bad_reads.append(input_dir+"/"+".".join(data_file.split(".")[:-1]))

        os.chdir(old_wd)
        return bad_reads

    def log_after_qc_version(self):
        '''
        logs version for after_qc
        '''
        version = "after.py version "\
                  + str(subprocess.check_output("after.py --version", shell=True)).split("\\n")[0][2:]
        print(version)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version, file=open_log_file)
        return


class FastQC(PipelineTool):
    def __init__(self, log_file, executable):
        super(FastQC, self).__init__(log_file, executable)
        self.log_fast_qc_version()

    def fastQC(self, outdir, reads, casava=False, nofilter=False, extract=False, java=None, noextract=False,
               nogroup=False, format=None, threads=1, contaminants=None, adapters=None, limits=None, kmers=None,
               quiet=False, dir=None):
        """
        Used for running fastqc
        NOTE: WHEN RUNNING THE FULL PIPELINE, DO NOT GIVE INPUT AND OUTPUT, THESE WILL BE INSERTED AUTOMATICALLY LATER.
        """
        extensions = ("fastq", "fastq", "fq")

        read_files = os.listdir(reads)
        reads = [reads + "/" + f for f in read_files if
                       f.split('.')[-1] in extensions]

        options = ["--casava " if casava else "",
                   "--nofilter" if nofilter else "",
                   "--extract" if extract else "",
                   "--noextract" if noextract else "",
                   "--nogroup" if nogroup else "",
                   "--quiet" if quiet else "",
                   "--java " + str(java) if java else "",
                   "--format " + str(format) if format else "",
                   "--contaminants " + str(contaminants) if contaminants else "",
                   "--adapters " + str(adapters) if adapters else "",
                   "--limits " + str(limits) if limits else "",
                   "--kmers " + str(kmers) if kmers else "",
                   ]

        exec_string = (self.executable + " --outdir " + str(outdir) + " ".join(options)
                       + " --threads " + str(threads) + " ".join(reads))

        log = "Executing FastQC with following command; " + exec_string
        print(log)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(log, file=open_log_file)

        os.system(exec_string)

        return


    def log_fast_qc_version(self):
        '''
        logs version for after_qc
        '''
        version = str(subprocess.check_output(self.executable + " --version", shell=True)).split("\\n")[0][2:]
        print(version)
        if self.log_file:
            with open(self.log_file, "a") as open_log_file:
                print(version, file=open_log_file)
        return


# functions

def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    print("This is a subpackage, it is only meant for being imported.")
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())