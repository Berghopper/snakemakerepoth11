R version 3.3.3 (2017-03-06)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux 9 (stretch)

locale:
 [1] LC_CTYPE=nl_NL.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=nl_NL.UTF-8        LC_COLLATE=C              
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=nl_NL.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] parallel  stats4    methods   stats     graphics  grDevices utils    
[8] datasets  base     

other attached packages:
[1] DESeq2_1.14.1              SummarizedExperiment_1.4.0
[3] Biobase_2.34.0             GenomicRanges_1.26.4      
[5] GenomeInfoDb_1.10.3        IRanges_2.8.2             
[7] S4Vectors_0.12.2           BiocGenerics_0.20.0       

loaded via a namespace (and not attached):
 [1] genefilter_1.56.0    locfit_1.5-9.1       splines_3.3.3       
 [4] lattice_0.20-34      colorspace_1.3-2     htmltools_0.3.6     
 [7] base64enc_0.1-3      blob_1.1.0           survival_2.40-1     
[10] XML_3.98-1.9         rlang_0.1.2          foreign_0.8-67      
[13] DBI_0.7              BiocParallel_1.8.2   bit64_0.9-7         
[16] RColorBrewer_1.1-2   plyr_1.8.4           stringr_1.2.0       
[19] zlibbioc_1.20.0      munsell_0.4.3        gtable_0.2.0        
[22] htmlwidgets_0.9      memoise_1.1.0        latticeExtra_0.6-28 
[25] knitr_1.16           geneplotter_1.52.0   AnnotationDbi_1.36.2
[28] htmlTable_1.9        Rcpp_0.12.12         acepack_1.4.1       
[31] xtable_1.8-2         scales_0.4.1         backports_1.1.0     
[34] checkmate_1.8.3      Hmisc_4.0-3          annotate_1.52.1     
[37] XVector_0.14.1       bit_1.1-12           gridExtra_2.2.1     
[40] ggplot2_2.2.1        digest_0.6.12        stringi_1.1.5       
[43] grid_3.3.3           bitops_1.0-6         tools_3.3.3         
[46] magrittr_1.5         lazyeval_0.2.0       RCurl_1.95-4.8      
[49] tibble_1.3.3         RSQLite_2.0          Formula_1.2-2       
[52] cluster_2.0.5        Matrix_1.2-8         data.table_1.10.4   
[55] rpart_4.1-10         nnet_7.3-12         
