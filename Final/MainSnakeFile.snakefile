# Main snake for running pipeline for all aligners

# Order of pipeline;
# --Concatting of genome fastas (IndecesGenerator.snakefile)
# --Building all indeces for hisat2, bwa and star. Of which star does not require the concatted file. (IndecesGenerator.snakefile)
# --Run mappings/alignings for hisat2, bwa and star for each sample. (AlignSamples.snakefile)
# --Make a Homer tag directory for each sample/aligner combo (QuantifyGenes.snakefile)
# --Quantify genes with homer for each result (QuantifyGenes.snakefile)
# --Run R script with all different homer quantification results and make some fancy-pants visualisations (Rcompare.snakefile)
# X-Run R script for summarising all comparisons into a heatmap (FAILED TO IMPLEMENT BECAUSE MISSING LIBRARY DEPENDENCY)
# --zip output of scsaAnalyse.R script. (Miscellaneous.snakefile)

# Load in config files

configfile: "config.yaml"

# Load in includes

include: "includes/Miscellaneous.snakefile"
include: "includes/IndecesGenerator.snakefile"
include: "includes/AlignSamples.snakefile"
include: "includes/QuantifyGenes.snakefile"
include: "includes/Rcompare.snakefile"

# rule all for calling zipped output and starting pipeline
rule all:
    input:
        config["zip_output"]
    output:
        config["empty_files_dir"]+"pipeline_done.txt"
    shell:
        "echo done > {output}"

# rule to reset everything of the pipeline
rule clean:
    shell:
        "rm -rf "+config["main_pipeline_dir"]

# clean indeces step.
rule clean_indeces:
    shell:
        "rm -rf "+config["main_pipeline_dir"]+"indeces && rm -rf "+config["empty_files_dir"]+"*index*"

# clean align step
rule clean_align:
    shell:
        "rm -rf "+config["main_pipeline_dir"]+"aligning && rm -rf "+config["empty_files_dir"]+"*align*"

# clean quantify homer step
rule clean_homer:
    shell:
        "rm -rf "+config["main_pipeline_dir"]+"homer"

# clean zip file
rule clean_zip:
    shell:
        "rm -rf "+config["zip_output"]

# clean output final file
rule clean_final:
    shell:
        "rm -rf "+config["empty_files_dir"]+"pipeline_done.txt"
