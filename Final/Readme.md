# Final assignment

In this assignment, I will reproduce a pipeline that I made earlier in colaboration with Robert Warmerdam that analyzes scRNA and RNA samples with multiple aligners to see which combination of aligners makes both datasets look most similar in result.


## Data used:

* Data from NCBI GEO Database (can be viewed in the old project paper);
```
# obtained with following command; ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/'

|-GSE79819
|---RNAseq
|-----brain_E12.5
|-----yolk_E12.5
|---scRNAseq
|-----brain_E12.5
|-----yolk_E12.5
```

## Files with code/other importance:

* MainSnakeFile.snakefile - this contains most includes to other snakefiles and starts the whole pipeline


## Snakefile outputs (can be found under succesful runs, 3 is the latest):

* All kinds of intermediary files such as SAM files and homer tag directories... (can be seen explained in the old project paper)
* Snakemake logs proving that the pipeline has ran
* An output zip of the R-analyzed samples (can be analyzed/summarized further with sortPipelines.R seperately)


## Used packages:

(output obtained with ```pip freeze --local``` while in the activated virtual env of the tutorial.)

```
appdirs==1.4.3
certifi==2018.1.18
chardet==3.0.4
ConfigArgParse==0.13.0
datrie==0.7.1
idna==2.6
pkg-resources==0.0.0
PyYAML==3.12
ratelimiter==1.2.0.post0
requests==2.18.4
snakemake==4.8.0
urllib3==1.22
wrapt==1.10.11
```


## Other resources

All other resources/info can be found under the old project and its respective paper (software used etc.) --> [repo.](https://bitbucket.org/dehakketakkers/th9_prj9_scsa/src)


-Casper Peters
