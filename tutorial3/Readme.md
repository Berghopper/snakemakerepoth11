# Tutorial 3

In this tutorial, we learn how to use remote files, and how to use special data paths and input files.

## Data used:

* bioinf.nl/~fennaf/snakemake/test.txt
* NCBI file; AAH32336.1.fasta in database protein
* NCBI file; KY785484.1.fasta in database nuccore
* Top 4 results of this NCBI query; ```"Zika virus"[Organism] AND (("9000"[SLEN] : "20000"[SLEN]) AND ("2017/03/20"[PDAT] : "2017/03/24"[PDAT])) ```

## Tutorial files with snakemake code:

* SnakefileHTML - containing part of downloading from html.
* SnakefileNCBI - containing the part about downloading with NCBI
* RemoteAccessSnakefile and RemoteAccessSnakefile2 - the snakefiles for the parts of code under the Remote access header.
* DataPathsExercise - the snakefile for the data path exercise
* InputFilesSnakefile, InputFilesSnakefile2 and InputFilesSnakefileConfig.yaml - the two seperate scripts and config file for the input files exercise.

## Snakefile outputs:

* tut3_DataPathsExercise - the output folder containing the output of the datapaths exercise
* tut3_InputFilesExercise - the output folder containing the output of the input files exercise

## Used packages:

(output obtained with ```pip freeze --local``` while in the activated virtual env of the tutorial.)

```
appdirs==1.4.3
biopython==1.70
certifi==2018.1.18
chardet==3.0.4
ConfigArgParse==0.13.0
datrie==0.7.1
docutils==0.14
idna==2.6
numpy==1.14.1
pkg-resources==0.0.0
PyYAML==3.12
ratelimiter==1.2.0.post0
requests==2.18.4
snakemake==4.7.0
urllib3==1.22
wrapt==1.10.11
```